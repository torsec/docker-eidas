# Docker images for eIDAS framework

## What is Docker?
Docker is an open platform for developers and sysadmins to build, ship, and 
run distributed applications. Consisting of Docker Engine, a portable, 
lightweight runtime and packaging tool, and Docker Hub, a cloud service for 
sharing applications and automating workflows, Docker enables apps to be 
quickly assembled from components and eliminates the friction between 
development, QA, and production environments. As a result, IT can ship faster 
and run the same app, unchanged, on laptops, data center VMs, and any cloud.

[https://www.docker.com/whatisdocker/](https://www.docker.com/whatisdocker/)

### What is a Docker Image?
Docker images are the basis of containers. Images are read-only, while 
containers are writeable. Only the containers can be executed by the operating
system.

[https://docs.docker.com/terms/image/](https://docs.docker.com/terms/image/)

## How to use these images?

At first, you need to install Docker Engine and Docker Compose plugin.

### Running the eIDAS infrastructure

The build process of the Docker images is automated by the Makefile. Issue the
following command in a Linux shell in order to build all the Docker images:

	$ make

You also have to configure the DNS in order to reach the eIDAS infrastructure
on the 'test.eidas.eu' hostname, which is the default configuration of
the modules. Therefore, you should add the following line in the `/etc/hosts`
configuration file:

	127.0.0.1 test.eidas.eu

As soon as your configuration is complete, you just need to run the following
command of Docker Composer plugin, which launches all the eIDAS modules and
shows logs in foreground:

	$ docker-compose up

