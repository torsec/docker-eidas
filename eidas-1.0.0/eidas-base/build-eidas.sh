#!/bin/bash
#
# The MIT License (MIT)
#
# Copyright (c) 2015 Paolo Smiraglia <paolo.smiraglia@polito.it>
#                    TORSEC Group (http://security.polito.it)
#                    Politecnico di Torino
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to
# deal in the Software without restriction, including without limitation the
# rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
# sell copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
# IN THE SOFTWARE.

TRUE=1
FALSE=0

# load rc file
_RC_FILE="./build-eidas.rc"
if [ -e $_RC_FILE ]; then
    source $_RC_FILE
fi

# ----------------------------------------------------------------------------
# logging helpers
# See: https://github.com/psmiraglia/misc/blob/master/bash/log.sh
# ----------------------------------------------------------------------------
# COLORS
__RESET="\e[0m"
__CYAN_BFG="\e[1;36m"
__CYAN_NFG="\e[0;36m"
__RED_BFG="\e[1;31m"
__RED_NFG="\e[0;31m"
__GREEN_BFG="\e[1;32m"
__GREEN_NFG="\e[0;32m"
__YELLOW_BFG="\e[1;33m"
__YELLOW_NFG="\e[0;33m"
__PURPLE_BFG="\e[1;35m"
__PURPLE_NFG="\e[0;35m"
__BLUE_BFG="\e[1;34m"
__BLUE_NFG="\e[0;34m"
__WHITE_BFG="\e[1;37m"
__WHITE_NFG="\e[0;37m"
__RED_BG="\e[41m"
# SETTINGS
# This will enable/disbale timestamp printing. Accepted values are 1 (enable)
# 0 (disable). Default value 0.
__LOG_WITH_TS=${LOG_WITH_TS:-0}
# This defines the TIMESPEC for ISO-8601 timestamp. Accepted values are
# 'date', 'hours', 'minutes', 'seconds' or 'ns'. Default value is 'seconds'.
__LOG_TS_FORMAT=${LOG_TS_FORMAT:-"seconds"}
# This will enable/disbale daemon mode. If enabled, messages will be forwarded
# to the system logger via 'logger'. Accepted values are 1 (enable)
# 0 (disable). Default value 0.
__LOG_AS_DAEMON=${LOG_AS_DAEMON:-0}
# This defines the daemon's name that will be used ad tag (-t).
# Default value is 'noname'.
__LOG_DAEMON_NAME=${LOG_DAEMON_NAME:-"noname"}
# This defines messages' threshold. Accepted values are 0 (emerg), 1 (alert),
# 2 (critical), 3 (error), 4 (warning), 5 (notice), 6 (info) and 7 (debug).
# Default value is 6 (info).
__LOG_THRESHOLD=${LOG_THRESHOLD:-6}
# This defines the syslog's facility if in daemon mode. Accepted values are
# those defined in "FACILITIES AND LEVELS" section for 'logger' man page.
# Default value is 'local0'.
__LOG_FACILITY=${LOG_FACILITY:-"local0"}
# logging helpers
function _log {
    local level=$1
    if [ $__LOG_WITH_TS -eq 0 ] || [ $__LOG_AS_DAEMON -eq 1 ]; then
        raw_msg="$2"
    else
        local ts=`date --iso-8601=$__LOG_TS_FORMAT`
        local raw_msg="$ts - $2"
    fi

    local tag=""
    local msg="$raw_msg"
    local syslog_level="info"
    local threshold=0
    case $level in
        debug)
            tag="${__CYAN_BFG}*${__RESET}"
            msg="${__CYAN_NFG}${raw_msg}${__RESET}"
            syslog_level="$level"
            threshold=7
            ;;
        info)
            tag="${__WHITE_BFG}*${__RESET}"
            msg="${__WHITE_NFG}${raw_msg}${__RESET}"
            syslog_level="$level"
            threshold=6
            ;;
        warning)
            tag="${__YELLOW_BFG}*${__RESET}"
            msg="${__YELLOW_NFG}${raw_msg}${__RESET}"
            syslog_level="$level"
            threshold=4
            ;;
        err)
            tag="${__RED_BFG}*${__RESET}"
            msg="${__RED_NFG}${raw_msg}${__RESET}"
            syslog_level="$level"
            threshold=3
            ;;
        emerg)
            tag="${__WHITE_BFG}${__RED_BG}*${__RESET}"
            msg="${__WHITE_BFG}${__RED_BG}${raw_msg}${__RESET}"
            syslog_level="err"
            threshold=0
            ;;
        success)
            tag="${__GREEN_BFG}*${__RESET}"
            msg="${__GREEN_NFG}${raw_msg}${__RESET}"
            syslog_level="info"
            threshold=5
            ;;
        failure)
            tag="${__RED_BFG}*${__RESET}"
            msg="${__RED_NFG}${raw_msg}${__RESET}"
            syslog_level="err"
            threshold=5
            ;;
        *)
            ;;
    esac

    if [ $__LOG_THRESHOLD -ge $threshold ]; then
        if [ $__LOG_AS_DAEMON -eq 0 ]; then
            echo -e "(${tag}) $msg"
        else
            logger -i -p ${__LOG_FACILITY}.${syslog_level} \
                -t $__LOG_DAEMON_NAME "$raw_msg"
        fi
    fi
}
# syslog related
function _debug { msg=$1 ; _log "debug" "$msg" ; }
function _info { msg=$1 ; _log "info" "$msg" ; }
function _warn { msg=$1 ; _log "warning" "$msg"; }
function _err { msg=$1 ; _log "err" "$msg" ; }
function _emerg { msg=$1 ; _log "emerg" "$msg" ; }
# custom (mapped on syslog levels)
function _success { msg=$1 ; _log "success" "$msg" ; }
function _failure { msg=$1 ; _log "failure" "$msg" ; }

#
# Identifying current Linux distribution ($os_dist)
#
_DEB="Debian"
_COS="CentOS"
_UBU="Ubuntu"
_UNDEF="undefined"
_DISTRO_LIST=($_DEB $_COS $_UBU $_UNDEF)

function __check_distro()
{
    local dist=$1
    local version_file="/proc/version"
    if [ -e $version_file ]; then
        echo `grep -ic $dist $version_file`
    fi
}

for os_dist in ${_DISTRO_LIST[@]}; do
    _debug "Checking for '$os_dist'"
    rc=`__check_distro $os_dist`
    if [ $rc -gt 0 ]; then
        break
    fi
done
_debug "Identified distribution is '$os_dist'"
#
# Checking dependencies
#

# Check if JAVA_HOME is defined
_debug "Checking \$JAVA_HOME"
if [ "$JAVA_HOME" == "" ]; then
    _err "\$JAVA_HOME is not set"
    exit 1
else
    _debug "\$JAVA_HOME set to '$JAVA_HOME'"
fi

# Check required binaries
IFS=':' read -a PATHS <<< "$PATH"
_DEP_MAVEN="mvn"
_DEP_JAVA="java"
_DEP_JAVAC="javac"
_DEP_LIST=( $_DEP_MAVEN $_DEP_JAVA $_DEP_JAVAC )

function __deb_install()
{
    local pkg=$1
    _info "Try that..."
    echo
    echo "    apt-get update"
    echo "    apt-get install $pkg"
    echo
}

function __rpm_install()
{
    local pkg=$1
    _info "Try that..."
    echo
    echo "    yum update"
    echo "    yum install $pkg"
    echo
}

for _dep in ${_DEP_LIST[@]}; do
    _debug "Checking '$_dep'"
    _found=0
    for _path in ${PATHS[@]}; do
        if [ -f "$_path/$_dep" ]; then
            _debug "Found '$_dep' in '$_path/$_dep'"
            _found=1
        fi
    done
    if [ $_found -eq 0 ]; then
        _failure "Missing dependency: $_dep"
        case $_dep in
            $_DEP_MAVEN)
                case $os_dist in
                    ($_DEB|$_UBU) __deb_install "maven" ;;
                    ($_COS) __rpm_install "maven" ;;
                esac
                ;;
            $_DEP_JAVA)
                case $os_dist in
                    ($_DEB|$_UBU) __deb_install "openjdk-7-jre" ;;
                    ($_COS) __rpm_install "java-1.7.0-openjdk" ;;
                esac
                ;;
            $_DEP_JAVAC)
                case $os_dist in
                    ($_DEB|$_UBU) __deb_install "openjdk-7-jdk" ;;
                    ($_COS) __rpm_install "java-1.7.0-openjdk-devel" ;;
                esac
                ;;
            *)
                _warn "No suggestions available. Good luck... :-)"
                ;;
        esac
        exit 1
    fi
done

# ----------------------------------------------------------------------------
# Maven helpers
# ----------------------------------------------------------------------------
_MVN_BIN=${MVN_BIN:-"/usr/bin/mvn"}
_MVN_OPTS=${MVN_OPTS:-""}
_MVN_REPO=${MVN_REPO:-"./MAVEN-LOCAL-REPO"}
_MVN_BASE_OPTS="-Dmaven.repo.local=$_MVN_REPO"
_MVN_BASE_OPTS="$_MVN_BASE_OPTS --fail-fast"
_MVN_RC=1

function _run_maven()
{
    local mvn_targets="$1"
    local mvn_opts="$_MVN_BASE_OPTS $_MVN_OPTS"
    _debug "Running Maven with options '$mvn_opts'"
    $_MVN_BIN $mvn_opts $mvn_targets
    _MVN_RC=$?
}

# ----------------------------------------------------------------------------
# Targets builders
# ----------------------------------------------------------------------------

_BASE_PATH=${BASE_PATH:-"."}

function __build_target()
{
    local _target_path=$1
    local _mvn_targets=$2
    if [ -d "$_BASE_PATH/$_target_path" ]; then
        pushd "$_BASE_PATH/$_target_path" > /dev/null
            _debug "Moved to `pwd`"
            _info "Building $_target_path"
            _run_maven "$_mvn_targets"
            if [ $_MVN_RC -ne 0 ]; then
                _failure "Something gone wrong in ${_target_path}..."
                exit 1
            else
                _success "$_target_path built successfully"
            fi
        popd > /dev/null
    else
        _err "Unable to locate '$_BASE_PATH/$_target_path'"
        exit 1
    fi
}

_with_parent=$FALSE
function _build_parent()
{
    __build_target "EIDAS-Parent" "clean install"
}

_with_commons=$FALSE
function _build_commons()
{
    __build_target "EIDAS-Commons" "clean install -P embedded"
}

_with_encryption=$FALSE
function _build_encryption()
{
    __build_target "EIDAS-Encryption" "clean install"
}

_with_configmodule=$FALSE
function _build_configmodule()
{
    __build_target "EIDAS-ConfigModule" "clean install"
}

_with_samlengine=$FALSE
function _build_samlengine()
{
    __build_target "EIDAS-SAMLEngine" "clean install"
}

_with_updater=$FALSE
function _build_updater()
{
    __build_target "EIDAS-UPDATER" "clean install"
}

_with_specific=$FALSE
function _build_specific()
{
    __build_target "EIDAS-Specific" "clean install -P embedded"
}

_with_node=$FALSE
function _build_node()
{
    __build_target "EIDAS-Node" "clean package -P embedded"
}

_with_sp=$FALSE
function _build_sp()
{
    __build_target "EIDAS-SP" "clean package -P embedded -P coreDependencies"
}

_with_idp=$FALSE
function _build_idp()
{
    __build_target "EIDAS-IdP-1.0" "clean package -P embedded"
}

_with_ap=$FALSE
function _build_ap()
{
    __build_target "EIDAS-AP" "clean package -P embedded"
}

function _reset_flags()
{
    _with_parent=$FALSE
    _with_commons=$FALSE
    _with_encryption=$FALSE
    _with_configmodule=$FALSE
    _with_samlengine=$FALSE
    _with_updater=$FALSE
    _with_specific=$FALSE
    _with_node=$FALSE
    _with_sp=$FALSE
    _with_idp=$FALSE
    _with_ap=$FALSE
}

# ----------------------------------------------------------------------------
# Help message
# ----------------------------------------------------------------------------
function print_help()
{
    cat <<EOL

Usage: `basename "$0"` [OPTIONS] [TARGETS]

OPTIONS may be one or more between

    -h, --help               Print the help message
    -c, --clean              Clean the eIDAS part of the local Maven repository
                             [$_MVN_REPO/eu/eidas]
    -C, --deep-clean         Clean the local Maven repository
                             [$_MVN_REPO]
    -v, --verbose            Enable verbose mode

TARGETS may be one or more between

    --all                    Build all modules
    --parent[-only]          Build [only] EIDAS-Parent
    --commons[-only]         Build [only] EIDAS-Commons
    --encryption[-only]      Build [only] EIDAS-Encryption
    --configmodule[-only]    Build [only] EIDAS-ConfigModule
    --samlengine[-only]      Build [only] EIDAS-SAMLEngine
    --updater[-only]         Build [only] EIDAS-Updater
    --specific[-only]        Build [only] EIDAS-Specific
    --node[-only]            Build [only] EIDAS-Node
    --sp[-only]              Build [only] EIDAS-SP
    --idp[-only]             Build [only] EIDAS-EIDAS-IdP-1.0
    --ap[-only]              Build [only] EIDAS-AP

Example:

    `basename "$0"` --all

EOL
    exit 0
}

# ----------------------------------------------------------------------------
# Options
# ----------------------------------------------------------------------------

if [ $# -eq 0 ]; then
    _err "Missing input arguments!!!"
    print_help
    exit 1
fi

_clean_repo=$FALSE
_deep_clean_repo=$FALSE
_be_verbose=$FALSE
optspec=":cCvh-:"
while getopts "$optspec" opt; do
    case $opt in
        (-)
            case "$OPTARG" in
                (node)
                    _with_parent=$TRUE
                    _with_commons=$TRUE
                    _with_encryption=$TRUE
                    _with_configmodule=$TRUE
                    _with_samlengine=$TRUE
                    _with_updater=$TRUE
                    _with_specific=$TRUE
                    _with_node=$TRUE
                    ;;
                (sp)
                    _with_parent=$TRUE
                    _with_commons=$TRUE
                    _with_encryption=$TRUE
                    _with_configmodule=$TRUE
                    _with_samlengine=$TRUE
                    _with_sp=$TRUE
                    ;;
                (ap)
                    _with_parent=$TRUE
                    _with_commons=$TRUE
                    _with_samlengine=$TRUE
                    _with_encryption=$TRUE
                    _with_configmodule=$TRUE
                    _with_ap=$TRUE
                    ;;
                (idp)
                    _with_parent=$TRUE
                    _with_commons=$TRUE
                    _with_encryption=$TRUE
                    _with_samlengine=$TRUE
                    _with_configmodule=$TRUE
                    _with_idp=$TRUE
                    ;;
                (all)
                    _with_parent=$TRUE
                    _with_commons=$TRUE
                    _with_encryption=$TRUE
                    _with_configmodule=$TRUE
                    _with_samlengine=$TRUE
                    _with_updater=$TRUE
                    _with_specific=$TRUE
                    _with_node=$TRUE
                    _with_sp=$TRUE
                    _with_idp=$TRUE
                    _with_ap=$TRUE
                    ;;
                (clean)
                    _clean_repo=$TRUE
                    ;;
                (deep-clean)
                    _deep_clean_repo=$TRUE
                    ;;
                (verbose)
                    _be_verbose=$TRUE
                    ;;
                (help)
                    print_help
                    exit 0
                    ;;
                (*)
                    _err "Invalid option: --$OPTARG"
                    print_help
                    exit 1
                    ;;
            esac
            ;;
        (c)
            _clean_repo=$TRUE
            ;;
        (C)
            _deep_clean_repo=$TRUE
            ;;
        (v)
            _be_verbose=$TRUE
            ;;
        (h)
            print_help
            exit 0
            ;;
        (\?)
            _err "Invalid option: -$OPTARG"
            print_help
            exit 1
            ;;
        (:)
            _err "Option -$OPTARG requires an argument"
            print_help
            exit 1
            ;;
    esac
done

if [ $_clean_repo -eq $TRUE ]; then
    _p="$_MVN_REPO/eu/eidas"
    _warn "Repository at '$_p' is going to be deleted"
    read -p "    Should I proceed? [y/N] " _a
    case $_a in
        (y|Y)
            rm -fr $_p
            _info "Deleted '$_p'"
            ;;
        (*)
            _info "Nothing was done..."
            ;;
    esac
fi

if [ $_deep_clean_repo -eq $TRUE ]; then
    _p="$_MVN_REPO"
    _warn "Repository at '$_p' is going to be deleted"
    read -p "    Should I proceed? [y/N] " _a
    case $_a in
        (y|Y)
            rm -fr $_p
            _info "Deleted '$_p'"
            ;;
        (*)
            _info "Nothing was done..."
            ;;
    esac
fi

if [ $_be_verbose -eq $TRUE ]; then
    _MVN_BASE_OPTS="$_MVN_BASE_OPTS -X -e"
fi

if [ $_with_parent -eq $TRUE ]; then
    _build_parent
fi

if [ $_with_commons -eq $TRUE ]; then
    _build_commons
fi

if [ $_with_encryption -eq $TRUE ]; then
    _build_encryption
fi

if [ $_with_configmodule -eq $TRUE ]; then
    _build_configmodule
fi

if [ $_with_samlengine -eq $TRUE ]; then
    _build_samlengine
fi

if [ $_with_updater -eq $TRUE ]; then
    _build_updater
fi

if [ $_with_specific -eq $TRUE ]; then
    _build_specific
fi

if [ $_with_node -eq $TRUE ]; then
    _build_node
fi

if [ $_with_sp -eq $TRUE ]; then
    _build_sp
fi

if [ $_with_idp -eq $TRUE ]; then
    _build_idp
fi

if [ $_with_ap -eq $TRUE ]; then
    _build_ap
fi
